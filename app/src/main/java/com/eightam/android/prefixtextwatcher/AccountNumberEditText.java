package com.eightam.android.prefixtextwatcher;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import static com.eightam.android.prefixtextwatcher.StringUtils.regroupWithSpaces;

public class AccountNumberEditText extends EditText {

    private String prefix;
    private int sectionWidth;
    private List<Integer> spaceIndices;
    private List<Integer> afterSpaceIndices;
    private StringBuilder sb;
    private TextWatcher textWatcher;

    public AccountNumberEditText(Context context) {
        super(context);
        initialize();
        initializeSpaceAndAfterSpaceIndices();
        bindInternalTextWatcher();
    }

    public AccountNumberEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeWithAttrs(attrs);
    }

    public AccountNumberEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeWithAttrs(attrs);
    }

    /**
     * Formula:
     * sectionWidth + (i - 1) * (sectionWidth + 1)
     * <p>
     * Example:
     * group: 1234 1234 1234 1234 1234 ...
     * index: 012345678901234567890123 ...
     * 4, 9, 14, 19, ...
     * <p>
     * i = 1 -> 4 + (1 - 1) * (4 + 1) =  4
     * i = 2 -> 4 + (2 - 1) * (4 + 1) =  9
     * i = 3 -> 4 + (3 - 1) * (4 + 1) = 14
     * i = 4 -> 4 + (4 - 1) * (4 + 1) = 19
     * ...
     */
    private static List<Integer> getSpaceIndices(int sectionWidth, int maxLength) {
        List<Integer> indices = new ArrayList<>();
        int i = 1;
        int index = 0;

        while (index < maxLength) {
            index = sectionWidth + (i - 1) * (sectionWidth + 1);

            if (index < maxLength - 1) {
                indices.add(index);
            }
            i++;
        }
        return indices;
    }

    private static List<Integer> getAfterSpaceIndices(int sectionWidth, int maxLength) {
        List<Integer> indices = new ArrayList<>();
        int i = 1;
        int index = 0;

        while (index < maxLength) {
            index = sectionWidth + (i - 1) * (sectionWidth + 1) + 1;

            if (index < maxLength) {
                indices.add(index);
            }
            i++;
        }
        return indices;
    }

    @Override
    public void setFilters(InputFilter[] filters) {
        super.setFilters(filters);
        initializeSpaceAndAfterSpaceIndices();
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;

        if (!TextUtils.isEmpty(this.prefix)) {
            setText(this.prefix);
            setSelection(length());
        }
    }

    public void setSectionWidth(int sectionWidth) {
        this.sectionWidth = sectionWidth;
        initializeSpaceAndAfterSpaceIndices();
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);

        if (!TextUtils.isEmpty(prefix)) {
            if (selStart >= 0 && selStart < prefix.length()) {
                int selection = Math.min(length(), prefix.length());
                setSelection(selection);
            }
        }
    }

    private void initialize() {
        sb = new StringBuilder();
        textWatcher = new InternalTextWatcher();

        setCustomSelectionActionModeCallback(new DisabledSelectionActionModeCallback());
        setLongClickable(false);
        setTextIsSelectable(false);
        setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    private void initializeWithAttrs(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AccountNumberEditText);
        String attrPrefix = a.getString(R.styleable.AccountNumberEditText_prefix);
        int attrSectionWidth = a.getInt(R.styleable.AccountNumberEditText_sectionWidth, 0);

        a.recycle();

        initialize();
        setPrefix(attrPrefix);
        setSectionWidth(attrSectionWidth);

        initializeSpaceAndAfterSpaceIndices();
        bindInternalTextWatcher();
    }

    private void initializeSpaceAndAfterSpaceIndices() {
        spaceIndices = new ArrayList<>();
        afterSpaceIndices = new ArrayList<>();

        InputFilter[] filters = getFilters();

        for (InputFilter filter : filters) {
            if (filter instanceof InputFilter.LengthFilter) {
                int maxLength = ((InputFilter.LengthFilter) filter).getMax();
                spaceIndices = getSpaceIndices(sectionWidth, maxLength);
                afterSpaceIndices = getAfterSpaceIndices(sectionWidth, maxLength);
                break;
            }
        }
    }

    private void bindInternalTextWatcher() {
        addTextChangedListener(textWatcher);
    }

    private void unbindInternalTextWatcher() {
        removeTextChangedListener(textWatcher);
    }

    class InternalTextWatcher extends AbstractTextWatcher {

        boolean startInPrefix;
        int beforeLength;
        int changeStart;
        int changeCount;
        int changeAfter;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            startInPrefix = start <= prefix.length();
            beforeLength = s.length();
            changeStart = start;
            changeCount = count;
            changeAfter = after;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!TextUtils.isEmpty(prefix)) {
                final String value = s.toString();

                if (!value.startsWith(prefix)) {
                    sb.setLength(0);

                    if (startInPrefix) {
                        sb.append(prefix);

                        // If selection is enabled then we need to consider 'count' & 'after'.
                        sb.append(value.substring(changeStart));

                        regroupWithSpaces(sb, sectionWidth);

                        setText(sb.toString());
                        setSelection(prefix.length());
                    } else {
                        sb.append(prefix);
                        regroupWithSpaces(sb, sectionWidth);

                        setText(sb.toString());
                        setSelection(length());
                    }
                } else {
                    sb.setLength(0);
                    sb.append(value);

                    regroupWithSpaces(sb, sectionWidth);

                    final String newValue = sb.toString();

                    if (!value.equals(newValue)) {
                        // Prevent the TextWatcher to be called during update.
                        unbindInternalTextWatcher();

                        setText(newValue);

                        if (changeStart > 0) {
                            int selection;

                            if (changeAfter > 0) {
                                // Adding new character (after > 0).
                                if (spaceIndices.contains(changeStart)) {
                                    // The case where space is inserted after adding new character.
                                    selection = changeStart + (changeAfter - changeCount) + 1;
                                } else {
                                    // The case where space is *not* inserted after adding new character.
                                    selection = changeStart + (changeAfter - changeCount);
                                }
                            } else if (changeAfter == 0) {
                                // Removing character (after == 0).
                                if (afterSpaceIndices.contains(changeStart)) {
                                    // The case where space is removed after removing character.
                                    selection = changeStart - 1;
                                } else {
                                    // The case where space is *not* removed after removing character.
                                    selection = changeStart;
                                }
                            } else {
                                selection = length();
                            }

                            selection = Math.max(prefix.length(), Math.min(length(), selection));
                            setSelection(selection);
                        }

                        // Restore the TextWatcher to listen to text change.
                        bindInternalTextWatcher();
                    }
                }
            }
        }

    }

}
