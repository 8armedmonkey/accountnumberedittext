package com.eightam.android.prefixtextwatcher;

public class StringUtils {

    private static final char CHAR_SPACE = ' ';

    public static String insertSpaces(String original, int sectionWidth) {
        StringBuilder sb = new StringBuilder(original);
        insertSpaces(sb, sectionWidth);

        return sb.toString();
    }

    public static void insertSpaces(StringBuilder sb, int sectionWidth) {
        if (sectionWidth > 0) {
            int charSectionCount = 0;
            int i = 0;

            while (i < sb.length()) {
                if (sb.charAt(i) != CHAR_SPACE) {
                    charSectionCount++;
                } else {
                    charSectionCount = 0;
                }

                if (charSectionCount > sectionWidth && i < sb.length()) {
                    sb.insert(i, CHAR_SPACE);
                    charSectionCount = 0;
                }

                i++;
            }
        }
    }

    public static String removeSpaces(String original) {
        return original.replaceAll("\\s+", "");
    }

    public static void removeSpaces(StringBuilder sb) {
        int i = 0;

        while (i < sb.length()) {
            if (sb.charAt(i) == CHAR_SPACE) {
                sb.deleteCharAt(i);
            } else {
                i++;
            }
        }
    }

    public static void regroupWithSpaces(StringBuilder sb, int sectionWidth) {
        removeSpaces(sb);
        insertSpaces(sb, sectionWidth);
    }

}
