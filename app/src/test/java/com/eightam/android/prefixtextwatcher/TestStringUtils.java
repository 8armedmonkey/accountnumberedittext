package com.eightam.android.prefixtextwatcher;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStringUtils {

    @Test
    public void insertSpaces_originalStringAndSectionWidth_stringWithSpacesEverySectionWidth1() {
        String original = "1234123412341234";
        String expected = "1234 1234 1234 1234";
        String actual = StringUtils.insertSpaces(original, 4);

        assertEquals(expected, actual);
    }

    @Test
    public void insertSpaces_originalStringAndSectionWidth_stringWithSpacesEverySectionWidth2() {
        String original = "1234 1234 1";
        String expected = "1234 1234 1";
        String actual = StringUtils.insertSpaces(original, 4);

        assertEquals(expected, actual);
    }

    @Test
    public void removeSpaces_originalString_stringWithoutSpaces() {
        String original = "1234 1234 1234 1234";
        String expected = "1234123412341234";
        String actual = StringUtils.removeSpaces(original);

        assertEquals(expected, actual);
    }

    @Test
    public void removeSpaces_stringBuilder_stringWithoutSpaces1() {
        StringBuilder sb = new StringBuilder("1234 1234 1234 1234");
        String expected = "1234123412341234";

        StringUtils.removeSpaces(sb);
        String actual = sb.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void removeSpaces_stringBuilder_stringWithoutSpaces2() {
        StringBuilder sb = new StringBuilder("1234123412341234");
        String expected = "1234123412341234";

        StringUtils.removeSpaces(sb);
        String actual = sb.toString();

        assertEquals(expected, actual);
    }

}
